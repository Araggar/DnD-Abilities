from random import randint
import numpy as np
import matplotlib.pyplot as plt

def roll_4d6_drop():
	a = np.array([randint(1,6) for i in range(4)])
	a = np.sort(a)
	return a[1:]

def roll_stats():
	stats = []
	for i in range(6):
		stats.append(sum(roll_4d6_drop()))
	return np.array(stats)

def players_stats(PLAYERS):
	return [roll_stats() for i in range(PLAYERS)]

def snake_pool(l):
	pool = np.reshape(l, len(l)*len(l[0]))
	return pool

def build_snake_stats(l, PLAYERS):
	stats = np.empty([PLAYERS, 6])
	l[::-1].sort()
	inc = 1
	for i in range(PLAYERS):
		_next = i
		first = True
		for j in range(6):
			stats[i][j] = l[_next]
			inc = (2*PLAYERS -i -1 -i) if (inc == (2*i)+1 or first) else (2*i)+1
			_next = _next+inc if (inc == (2*i)+1 or first) else _next+inc
			first = False
		inc = i+1
	return stats


if __name__ == '__main__':

	PLAYERS = 4
	SAMPLE_SIZE = 10000

	player_sets = players_stats(PLAYERS)

	pool = snake_pool(player_sets)
	np.sort(pool)
	snake_player_set = build_snake_stats(pool, PLAYERS)

	std_4d6_sum = [sum(i) for i in player_sets]
	snake_sum = [sum(i) for i in snake_player_set]
	
	std_dev_standard_sum = np.std(std_4d6_sum)
	std_dev_socialist_sum = np.std(snake_sum)
	std_dev_standard = np.std(player_sets)
	std_dev_socialist = np.std(snake_player_set)

	print("Player Status 4d6 drop lowest: \n", np.sort(player_sets))
	print("Stat Sums by Player: ", std_4d6_sum)
	print("Standard Deviantion for 4d6 drop lowest: ",std_dev_standard)
	print("Standard Deviantion for sum 4d6 drop lowest: ",std_dev_standard_sum)

	print("Player Status Snake Draft: \n", snake_player_set)
	print("Stat Sums by Player: ", snake_sum)
	print("Standard Deviantion for Snake Draft: ",std_dev_socialist)
	print("Standard Deviantion for Sum Snake Draft: ",std_dev_socialist_sum)

	print("\nStandard Deviation Ratio: (4d6/Snake)", std_dev_standard / std_dev_socialist)


	plt.ylabel("Sum of Abilities Scores")

	std_4dp_plot_points = []
	snake_draft_plot_points = []

	for i in range(1000):
		for j in players_stats(PLAYERS):
			std_4dp_plot_points.append(sum(j))
		for j in build_snake_stats(snake_pool(players_stats(PLAYERS)), PLAYERS):
			snake_draft_plot_points.append(sum(j))


	plt.scatter([1]*len(std_4dp_plot_points), std_4dp_plot_points)
	plt.scatter([2]*len(snake_draft_plot_points), snake_draft_plot_points)

	plt.xticks([0,1,2,3], ["","4d6 drop","Snake Draft", ""])

	plt.show()